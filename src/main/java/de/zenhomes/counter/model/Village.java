package de.zenhomes.counter.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;

import de.zenhomes.counter.dto.VillageDTO;
import lombok.Data;

@Data
@Entity
public class Village extends BaseEntity<VillageDTO>{

	@NotBlank
	@Column(nullable = false)
	private String name;
	
	@OneToMany(mappedBy = "village", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Counter> counters;
	
	@Override
	public VillageDTO toDTO() {
		VillageDTO villageDTO = new VillageDTO();
		villageDTO.setId(this.getId());
		villageDTO.setName(this.getName());
		return villageDTO;
	}

	@Override
	public void fromDTO(VillageDTO dto) {
		this.setName(dto.getName());
	}

}
