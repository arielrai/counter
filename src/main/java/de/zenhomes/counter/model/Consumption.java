package de.zenhomes.counter.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import de.zenhomes.counter.dto.ConsumptionDTO;
import lombok.Data;

@Data
@Entity
public class Consumption extends BaseEntity<ConsumptionDTO> {
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "counter_id", nullable = false)
	private Counter counter;
	
	@Column(nullable = false)
	private BigDecimal amount;
	
	@Column(nullable = false)
	private LocalDateTime dateOfConsumption = LocalDateTime.now(); 
	
	@Override
	public ConsumptionDTO toDTO() {
		ConsumptionDTO consumptionDTO = new ConsumptionDTO();
		consumptionDTO.setId(this.getId());
		consumptionDTO.setAmount(this.amount.doubleValue());
		consumptionDTO.setDateOfConsumption(this.dateOfConsumption);
		return consumptionDTO;
	}

	@Override
	public void fromDTO(ConsumptionDTO dto) {
		this.setAmount(new BigDecimal(dto.getAmount()));
		this.setDateOfConsumption(dto.getDateOfConsumption());
	}

}
