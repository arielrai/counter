package de.zenhomes.counter.model;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import de.zenhomes.counter.dto.CounterDTO;
import lombok.Data;

@Data
@Entity
public class Counter extends BaseEntity<CounterDTO> {

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "village", nullable = false)
	private Village village;
	
	@OneToMany(mappedBy = "counter", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Consumption> consumptions;
	
	@Override
	public CounterDTO toDTO() {
		CounterDTO counterDTO = new CounterDTO();
		counterDTO.setId(this.getId());
		counterDTO.setVillageName(this.village.getName());
		counterDTO.setConsumptions(this.consumptions.stream().map(Consumption::toDTO).collect(Collectors.toList()));
		return counterDTO;
	}

	@Override
	public void fromDTO(CounterDTO dto) {
	}

}
