package de.zenhomes.counter.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import de.zenhomes.counter.model.Consumption;

public interface ConsumptionRepository extends JpaRepository<Consumption, Long> {
	
	List<Consumption> findAllByCounterIdAndDateOfConsumptionLessThanEqualAndDateOfConsumptionGreaterThanEqual(Long counterId, 
			LocalDateTime endDate, LocalDateTime startDate);

	
}
