package de.zenhomes.counter.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import de.zenhomes.counter.model.Counter;

public interface CounterRepository extends JpaRepository<Counter, Long> {
	
	List<Counter> findAllByVillageId(Long id);
}
