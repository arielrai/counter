package de.zenhomes.counter.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import de.zenhomes.counter.model.Village;

public interface VillageRepository extends JpaRepository<Village, Long> {
	
}
