package de.zenhomes.counter.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.zenhomes.counter.dto.ReportDTO;
import de.zenhomes.counter.dto.ReportVillageDTO;
import de.zenhomes.counter.dto.VillageDTO;
import de.zenhomes.counter.model.Counter;
import de.zenhomes.counter.model.Village;
import de.zenhomes.counter.repository.ConsumptionRepository;
import de.zenhomes.counter.repository.CounterRepository;
import de.zenhomes.counter.repository.VillageRepository;
import javassist.NotFoundException;

@Service
public class VillageService {
	
	@Autowired private VillageRepository villageRepository;
	@Autowired private CounterRepository counterRepository;
	@Autowired private ConsumptionRepository consumptionRepository;
	
	@Transactional
	public Long createVillage(VillageDTO villageDto){
		Village village = new Village();
		village.fromDTO(villageDto);
		return villageRepository.save(village).getId();
	}
	
	@Transactional
	public VillageDTO findVillage(Long villageId) throws NotFoundException{
		Optional<Village> optionalVillage = villageRepository.findById(villageId);
		if (optionalVillage.isPresent()) {
			return optionalVillage.get().toDTO();
		} else {
			throw new NotFoundException(generateNotFoundMessage(villageId));
		}
	}

	private String generateNotFoundMessage(Long villageId) {
		return String.format("%s with id %s not found", Village.class.getSimpleName(), villageId);
	}
	
	@Transactional
	public Long createCounter(Long villageId) throws NotFoundException {
		Optional<Village> villageOptional = villageRepository.findById(villageId);
		if (villageOptional.isPresent()) {
			Counter counter = new Counter();
			counter.setVillage(villageOptional.get());
			return counterRepository.save(counter).getId();
		} else {
			throw new NotFoundException(generateNotFoundMessage(villageId));
		}
	}
	
	@Transactional
	public ReportDTO generateReport(final int hours) {
		List<ReportVillageDTO> reportPerVillage = villageRepository.findAll().stream().map(village -> {
			Double sumOfCounters = village.getCounters().stream().map(
					arg0 -> consumptionRepository.findAllByCounterIdAndDateOfConsumptionLessThanEqualAndDateOfConsumptionGreaterThanEqual(
							arg0.getId(), LocalDateTime.now(), LocalDateTime.now().minusHours(hours)).stream()
					.collect(Collectors.summingDouble(c -> c.getAmount().doubleValue())))
					.collect(Collectors.summingDouble(d -> d));
			ReportVillageDTO reportVillageDTO = new ReportVillageDTO();
			reportVillageDTO.setVillageName(village.getName());
			reportVillageDTO.setConsumption(sumOfCounters);
			return reportVillageDTO;
		}).collect(Collectors.toList());
		ReportDTO reportDTO = new ReportDTO();
		reportDTO.setVillages(reportPerVillage);
		return reportDTO;
	}
}
