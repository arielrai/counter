package de.zenhomes.counter.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.zenhomes.counter.dto.ConsumptionDTO;
import de.zenhomes.counter.dto.CounterDTO;
import de.zenhomes.counter.model.Consumption;
import de.zenhomes.counter.model.Counter;
import de.zenhomes.counter.repository.CounterRepository;
import javassist.NotFoundException;

@Service
public class CounterService {

	@Autowired private CounterRepository counterRepository;
	
	@Transactional
	public CounterDTO updateCounter(Long counterId, ConsumptionDTO consumptionDto) throws NotFoundException {
		Consumption consumption = new Consumption();
		consumption.fromDTO(consumptionDto);
		
		Optional<Counter> optionalCounter = counterRepository.findById(counterId);
		if (optionalCounter.isPresent()) {
			Counter entity = optionalCounter.get();
			Consumption consumptionEntity = new Consumption();
			consumptionEntity.fromDTO(consumptionDto);
			consumptionEntity.setCounter(entity);
			entity.getConsumptions().add(consumptionEntity);
			return counterRepository.save(entity).toDTO();
		} else {
			throw new NotFoundException(String.format("%s with id %s not found", Counter.class.getSimpleName(), counterId));
		}
	}
	
	@Transactional
	public CounterDTO findCounter(Long counterId) throws NotFoundException {
		Optional<Counter> optionalCounter = counterRepository.findById(counterId);
		if (optionalCounter.isPresent()) {
			return optionalCounter.get().toDTO();
		} else {
			throw new NotFoundException(String.format("%s with id %s not found", Counter.class.getSimpleName(), counterId));
		}
	}
}
