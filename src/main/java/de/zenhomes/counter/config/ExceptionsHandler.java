package de.zenhomes.counter.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javassist.NotFoundException;

/**
 * Exception handling for converting various application errors into matching
 * HTTP status responses.
 *
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
@RestControllerAdvice
public class ExceptionsHandler extends ResponseEntityExceptionHandler {

	private static final Logger logger = LoggerFactory.getLogger(ExceptionsHandler.class);
	
	@ExceptionHandler(NotFoundException.class)
	public final ResponseEntity notFoundHandler(NotFoundException ex) {
		logger.error(ex.getMessage());
		return ResponseEntity.notFound().build();
	}
	
}
