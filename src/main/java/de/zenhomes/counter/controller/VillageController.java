package de.zenhomes.counter.controller;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import de.zenhomes.counter.dto.ConsumptionDTO;
import de.zenhomes.counter.dto.CounterDTO;
import de.zenhomes.counter.dto.ReportDTO;
import de.zenhomes.counter.dto.VillageDTO;
import de.zenhomes.counter.service.CounterService;
import de.zenhomes.counter.service.URLBuilderService;
import de.zenhomes.counter.service.VillageService;
import javassist.NotFoundException;

@RestController
@RequestMapping("villages")
public class VillageController {

	@Autowired 
	private VillageService villageService;
	@Autowired 
	private CounterService counterService;
	@Autowired private URLBuilderService urlBuilderService;
	
	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public URI createVillage(@RequestBody VillageDTO village) {
		return urlBuilderService.buildCreateURL(villageService.createVillage(village));
	}
	
	@GetMapping("/{id}")
	public VillageDTO getVillage(@PathVariable("id") Long id) throws NotFoundException {
		return villageService.findVillage(id);
	}
	
	@PostMapping("/{id}/counters")
	@ResponseStatus(code = HttpStatus.CREATED)
	public URI createCounter(@PathVariable("id") Long id) throws NotFoundException {
		return urlBuilderService.buildCreateURL(villageService.createCounter(id));
	}
	
	//TODO this could maybe be extract to a different controller
	@GetMapping("/{id}/counters/{idCounter}")
	public CounterDTO getCounter(@PathVariable("id") Long id, @PathVariable("idCounter") Long idCounter) throws NotFoundException {
		return counterService.findCounter(idCounter);
	}
	
	@PostMapping("/{id}/counters/{idCounter}/update_counter")
	public CounterDTO updateCounter(@PathVariable("id") Long id, @PathVariable("idCounter") Long idCounter, @RequestBody ConsumptionDTO consumption) throws NotFoundException {
		return counterService.updateCounter(idCounter, consumption);
	}
	
	@GetMapping("/consumption_report")
	public ReportDTO generateReport(@RequestParam(value = "duration") Integer duration) {
		return villageService.generateReport(duration);
	}
}
