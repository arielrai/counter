package de.zenhomes.counter.dto;

import java.util.List;

import lombok.Data;

@Data
public class ReportDTO {

	private List<ReportVillageDTO> villages;
}
