package de.zenhomes.counter.dto;

import lombok.Data;

@Data
public class ReportVillageDTO {

	private String villageName;
	private Double consumption;
}
