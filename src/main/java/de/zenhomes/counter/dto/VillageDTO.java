 package de.zenhomes.counter.dto;

import java.util.List;

import lombok.Data;

@Data
public class VillageDTO extends BaseDTO {
	
	private String name;
	private List<CounterDTO> counter;
}
