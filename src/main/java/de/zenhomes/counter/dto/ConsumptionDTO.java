package de.zenhomes.counter.dto;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class ConsumptionDTO extends BaseDTO {

	private LocalDateTime dateOfConsumption = LocalDateTime.now();
	private Double amount;
}
