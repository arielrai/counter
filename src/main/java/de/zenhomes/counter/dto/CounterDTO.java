package de.zenhomes.counter.dto;

import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
public class CounterDTO extends BaseDTO {

	private List<ConsumptionDTO> consumptions;
	private String villageName;

	public Double getAmount() {
		if (consumptions == null || consumptions.isEmpty()) {
			return 0D;
		}
		return consumptions.stream().collect(Collectors.summingDouble(c -> c.getAmount().doubleValue()));
	}
}
