package de.zenhomes.counter.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.net.URI;
import java.time.LocalDateTime;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import de.zenhomes.counter.dto.ConsumptionDTO;
import de.zenhomes.counter.dto.CounterDTO;
import de.zenhomes.counter.dto.ReportDTO;
import de.zenhomes.counter.dto.VillageDTO;
import de.zenhomes.counter.repository.CounterRepository;
import de.zenhomes.counter.repository.VillageRepository;
import de.zenhomes.counter.service.CounterService;
import de.zenhomes.counter.utils.RequestUtils;
import javassist.NotFoundException;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class VillageControllerTest {

	private static final String villageName = "Random Village";
	@Autowired
	private TestRestTemplate restTemplate;
	@Autowired
	private VillageRepository villageRepository;
	@Autowired
	private CounterRepository counterRepository;
	@Autowired
	private CounterService counterService;
	
	@AfterEach
	public void cleanUp() {
		villageRepository.deleteAll();
		counterRepository.deleteAll();
	}
	
	@Test
	public void shouldCreateVillageAndFindSameVillage() throws Exception {
		String json = "{\"name\" : \"" + villageName + "\"}";
		ResponseEntity<URI> response = restTemplate.postForEntity("/villages", 
				RequestUtils.buildJsonRequest(json),
				URI.class);	
		
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		
		ResponseEntity<VillageDTO> villageResponseEntity = restTemplate.getForEntity(response.getBody(), VillageDTO.class);
		assertEquals(HttpStatus.OK, villageResponseEntity.getStatusCode());
		assertEquals(villageName, villageResponseEntity.getBody().getName());
	}
	
	@Test
	public void shouldExpect404OfNonExistingVillage() throws Exception {
		ResponseEntity<VillageDTO> response = restTemplate.getForEntity("/villages/35",
				VillageDTO.class);	
		
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}
	
	@Test
	public void shouldCreateCounterFindAndUpdate() throws Exception {
		String json = "{\"name\" : \"" + villageName + "\"}";
		ResponseEntity<URI> response = restTemplate.postForEntity("/villages", 
				RequestUtils.buildJsonRequest(json),
				URI.class);	
		
		ResponseEntity<VillageDTO> villageResponseEntity = restTemplate.getForEntity(response.getBody(), VillageDTO.class);
		
		ResponseEntity<URI> responseCounter = restTemplate.postForEntity("/villages/"+ villageResponseEntity.getBody().getId() + "/counters", 
				null,
				URI.class);
		
		ResponseEntity<CounterDTO> counterResponseEntity = restTemplate.getForEntity(responseCounter.getBody(), CounterDTO.class);
		assertEquals(villageName, counterResponseEntity.getBody().getVillageName());
		assertEquals(0D, counterResponseEntity.getBody().getAmount());
		
		
		counterResponseEntity = restTemplate.postForEntity("/villages/"+ villageResponseEntity.getBody().getId() + "/counters/"+counterResponseEntity.getBody().getId() + "/update_counter",
					RequestUtils.buildJsonRequest("{\"amount\":50.0}"),
					CounterDTO.class);
		assertEquals(villageName, counterResponseEntity.getBody().getVillageName());
		assertEquals(50D, counterResponseEntity.getBody().getAmount());
	}
	
	@Test
	public void shouldExpect404OfNonExistingCounter() throws Exception {
		String json = "{\"name\" : \"" + villageName + "\"}";
		ResponseEntity<URI> response = restTemplate.postForEntity("/villages", 
				RequestUtils.buildJsonRequest(json),
				URI.class);	
		
		ResponseEntity<VillageDTO> villageResponseEntity = restTemplate.getForEntity(response.getBody(), VillageDTO.class);
		ResponseEntity<CounterDTO> responseCounter = restTemplate.getForEntity("/villages/" + villageResponseEntity.getBody().getId() + "/counters/12",
				CounterDTO.class);	
		
		assertEquals(HttpStatus.NOT_FOUND, responseCounter.getStatusCode());
	}
	
	@Test
	public void shouldExpect404OfOnUpdateNonExistingCounter() throws Exception {
		String json = "{\"name\" : \"" + villageName + "\"}";
		ResponseEntity<URI> response = restTemplate.postForEntity("/villages", 
				RequestUtils.buildJsonRequest(json),
				URI.class);	
		
		ResponseEntity<VillageDTO> villageResponseEntity = restTemplate.getForEntity(response.getBody(), VillageDTO.class);
		ResponseEntity<CounterDTO> responseCounter = restTemplate.postForEntity("/villages/" + villageResponseEntity.getBody().getId() + "/counters/12/update_counter",
				RequestUtils.buildJsonRequest("{\"amount\":50.0}"),
				CounterDTO.class);	
		
		assertEquals(HttpStatus.NOT_FOUND, responseCounter.getStatusCode());
	}
	
	@Test
	public void shouldGenerateReport() throws NotFoundException {

		String json = "{\"name\" : \"" + villageName + "\"}";
		ResponseEntity<URI> response = restTemplate.postForEntity("/villages", 
				RequestUtils.buildJsonRequest(json),
				URI.class);	
		
		ResponseEntity<VillageDTO> villageResponseEntity = restTemplate.getForEntity(response.getBody(), VillageDTO.class);
		
		ResponseEntity<URI> responseCounter = restTemplate.postForEntity("/villages/"+ villageResponseEntity.getBody().getId() + "/counters", 
				RequestUtils.buildJsonRequest("{}"),
				URI.class);
		
		ResponseEntity<CounterDTO> counterResponseEntity = restTemplate.getForEntity(responseCounter.getBody(), CounterDTO.class);
		
		counterResponseEntity = restTemplate.postForEntity("/villages/"+ villageResponseEntity.getBody().getId() + "/counters/"+counterResponseEntity.getBody().getId()+"/update_counter",
					RequestUtils.buildJsonRequest("{\"amount\":50}"),
					CounterDTO.class);
		
		//Includes consumption that should NOT be included in the report
		ConsumptionDTO consumptionDto = new ConsumptionDTO();
		consumptionDto.setAmount(300D);
		consumptionDto.setDateOfConsumption(LocalDateTime.now().minusHours(25));
		counterService.updateCounter(counterResponseEntity.getBody().getId(), consumptionDto);
		
		ResponseEntity<ReportDTO> reportResponse = restTemplate.getForEntity("/villages/consumption_report?duration=24", ReportDTO.class);
		assertEquals(50D, reportResponse.getBody().getVillages().get(0).getConsumption());
		assertEquals(villageName, reportResponse.getBody().getVillages().get(0).getVillageName());

		//Increase the duration of the Report
		reportResponse = restTemplate.getForEntity("/villages/consumption_report?duration=26", ReportDTO.class);
		assertEquals(350D, reportResponse.getBody().getVillages().get(0).getConsumption());
	}
}
