package de.zenhomes.counter.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.validation.ConstraintViolationException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;

import de.zenhomes.counter.dto.ConsumptionDTO;
import de.zenhomes.counter.dto.ReportDTO;
import de.zenhomes.counter.dto.VillageDTO;
import de.zenhomes.counter.repository.CounterRepository;
import de.zenhomes.counter.repository.VillageRepository;
import javassist.NotFoundException;

@SpringBootTest
public class ServiceTests {

	@Autowired
	public VillageService villageService;
	@Autowired
	public CounterService counterService;
	@Autowired
	public VillageRepository villageRepository;
	@Autowired
	public CounterRepository counterRepository;
	
	@AfterEach
	public void cleanUp() {
		villageRepository.deleteAll();
		counterRepository.deleteAll();
	}
	
	@Test
	public void shouldExpectNonNullIdOnCreateVillage() {
		VillageDTO villageDto = new VillageDTO();
		villageDto.setName("A random village");
		villageService.createVillage(villageDto);
		assertNotNull(villageService.createVillage(villageDto));
	}
	
	@Test
	public void shouldExpectExceptionOnCreateVillageWithoutName() {
		VillageDTO villageDto = new VillageDTO();
		assertThrows(ConstraintViolationException.class, () -> villageService.createVillage(villageDto));
	}
	
	@Test
	public void shouldExpectNonNullIdOnCreateCounter() throws NotFoundException {
		VillageDTO villageDto = new VillageDTO();
		villageDto.setName("A random village");

		Long villageId = villageService.createVillage(villageDto);
		assertNotNull(villageService.createCounter(villageId));
	}

	@Test
	public void shouldExpectAmountChangeAfterCountUpdate() throws NotFoundException {
		VillageDTO villageDto = new VillageDTO();
		villageDto.setName("A random village");

		Long counter = villageService.createCounter(villageService.createVillage(villageDto));
		ConsumptionDTO consumption = new ConsumptionDTO();
		consumption.setAmount(new BigDecimal(50).doubleValue());
		assertEquals(50D, counterService.updateCounter(counter, consumption).getAmount());
	}
	
	@Test
	public void shouldExpectExceptionOnNonExistingCounter() throws NotFoundException {
		ConsumptionDTO consumption = new ConsumptionDTO();
		consumption.setAmount(new BigDecimal(50).doubleValue());
		assertThrows(NotFoundException.class, () -> counterService.updateCounter(-1L, consumption));
	}
	
	@Test
	public void shouldExpectReportWithOneVillage() throws NotFoundException {

		VillageDTO villageDto = new VillageDTO();
		villageDto.setName("A random village");

		Long counter = villageService.createCounter(villageService.createVillage(villageDto));
		ConsumptionDTO consumption = new ConsumptionDTO();
		consumption.setAmount(new BigDecimal(50).doubleValue());
		counterService.updateCounter(counter, consumption);
	
		ReportDTO generateReport = villageService.generateReport(24);
		assertEquals(1, generateReport.getVillages().size());
		assertEquals(villageDto.getName(), generateReport.getVillages().get(0).getVillageName());
		assertEquals(50D, generateReport.getVillages().get(0).getConsumption());
	}
	
	@Test
	public void shouldExpectReportWithTwoVillages() throws NotFoundException {

		VillageDTO villageDto = new VillageDTO();
		villageDto.setName("A random village");

		Long counter = villageService.createCounter(villageService.createVillage(villageDto));
		ConsumptionDTO consumption = new ConsumptionDTO();
		consumption.setAmount(new BigDecimal(50).doubleValue());
		counterService.updateCounter(counter, consumption);
		
		VillageDTO villageDto2 = new VillageDTO();
		villageDto2.setName("A random village 2");

		Long counter2 = villageService.createCounter(villageService.createVillage(villageDto2));
		ConsumptionDTO consumption2 = new ConsumptionDTO();
		consumption2.setAmount(new BigDecimal(60).doubleValue());
		counterService.updateCounter(counter2, consumption2);
	
		ReportDTO generateReport = villageService.generateReport(24);
		assertEquals(2, generateReport.getVillages().size());
		assertEquals(villageDto.getName(), generateReport.getVillages().get(0).getVillageName());
		assertEquals(50D, generateReport.getVillages().get(0).getConsumption());
		
		assertEquals(2, generateReport.getVillages().size());
		assertEquals(villageDto2.getName(), generateReport.getVillages().get(1).getVillageName());
		assertEquals(60D, generateReport.getVillages().get(1).getConsumption());
	}
	
	@Test
	public void shouldExpectReportWithValuesInProvidedTime() throws NotFoundException {

		VillageDTO villageDto = new VillageDTO();
		villageDto.setName("A random village");

		Long counter = villageService.createCounter(villageService.createVillage(villageDto));
		ConsumptionDTO consumption = new ConsumptionDTO();
		consumption.setAmount(new BigDecimal(50).doubleValue());
		consumption.setDateOfConsumption(LocalDateTime.now().minusHours(25));
		counterService.updateCounter(counter, consumption);
		
		consumption.setAmount(new BigDecimal(30).doubleValue());
		consumption.setDateOfConsumption(LocalDateTime.now().minusHours(23));
		counterService.updateCounter(counter, consumption);
	
		ReportDTO generateReport = villageService.generateReport(24);
		assertEquals(1, generateReport.getVillages().size());
		assertEquals(villageDto.getName(), generateReport.getVillages().get(0).getVillageName());
		assertEquals(30D, generateReport.getVillages().get(0).getConsumption());
	}
	
}
