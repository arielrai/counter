# Village counters

## About this project
### Application model
- For the Application model three classes were considered: Village, Counter and Consumption. The existance of Village and Counter are very clear. However, the existance of consumption was considered manly because it was required to save the date which the consumption was registered.
### System Architecture
- Very simple MVC application, where repositories are encapsulated in the services. Also all the business logic is in the service layer.
### Framwork choice
- Spring and JUnit. Both are very well known and have high adoption in the Java development community. Because of the high adoption is always easy to find good material to learn and understand how these frameworks work. Another plus is that, since it's highly adopted by the community you can always ask for support on forums, stackoverflow, etc when needed.
### Testing strategy 
- The Testing strategy was TDD. Basically all the tests were implemented based on what the system have to do, and after that the actual system was developed.

## How to run it? It is simple!
- <code>cd *'PROJECT_FOLDER'*</code>.
- <code>chmod 777 mvnw</code> (if the file doesn't have permission)
- <code>./mvnw clean package</code>
- <code>java -jar target/coding-counter-0.0.1-SNAPSHOT.jar</code>
- Done! The application is running on the port :8080
- While running you can check the API Documentation at: <http://localhost:8080/swagger-ui.html>

## How to test it? It is even simpler!
- <code>cd *'PROJECT_FOLDER'*</code>.
- <code>./mvnw clean test</code>.
- These two commands will run the tests

### Code Quality
- Check the code quality at <https://sonarcloud.io/dashboard?id=zenhomes-counter>

#### TODOs
- Add Deploy to gitlab pipeline
- Gitlab badges
